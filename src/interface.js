// # COLORS

const token_color = {
    "input" : "#8888", // gray
    "player" : "#fb0", // yellow
    "npc" : "#f70", // orange
    "quest" : "#a0f",  // ??? 
    "structure" : "#ab0", // cyan
    "item" : "#f0a", // pink
    "game" : "#000",  // black
    "subject" : "#0ab",  // black
    // "command" : "#00f", // blue
    "location" : "#0c0"  // green
};

const token = {
  "Quests" : "quest",
  "Quest" : "quest",
  "quests" : "quest",
  "quest" : "quest",
  "goblin" : "subject",
  "swamp" : "subject",
  "log"    : "subject",
}

for ( var i in token ) {
  var capitalized = i.capitalize();
  var plural_capitalized = i.capitalize()+'s';
  var plural = i+'s';
  token[capitalized] = token[i] 
  token[plural_capitalized] = token[i] 
  token[plural] = token[i] 
}

// # TERMINAL

const terminal = document.createElement("section");

terminal.id = "terminal";

document.body.appendChild(terminal);

// ## anim text

function init_anim(el){
  if (!el.textContent.length) return;
  el.to_anim = el.textContent;
  el.textContent = '';
  text_anim(el);
};

function text_anim(el){
  if (el.index >= el.to_anim.length ) return;
  if (!el.index) el.index= 0;
  el.textContent += el.to_anim[el.index];
  el.index ++;
  setTimeout(function(){
    text_anim(this);
  }.bind(el),Math.random()*50);
}

// ## LOG

function log(entry){
  for ( var i = 0 ; i < entry.length ; i++ ) {
    terminal.appendChild(entry[i]);
    for (var j = 0 ; j < entry[i].children.length ; j++ ){
      init_anim(entry[i].children[j]);
    };
  };
  for ( var i = terminal.children.length-1 ; i > 0 ; i-- ) {
    if ( i >= 20 ) { 
      terminal.children[terminal.children.length-1 - i ].style.display = "none";
    } else {
      terminal.children[terminal.children.length-1 - i ].style.display = "block";
    };
  };
};

// # CMD LINE

var cmd_str = new String();
var cursor = '█';

const cmd = document.createElement("p");

cmd.id = "cmd";

// ## DOM

document.body.appendChild(cmd);

// # KEYBOARD INPUT

document.addEventListener("keydown",function(e){
  e.preventDefault(); e.stopPropagation();
  var key
  if ( e.key == "Dead" ) {
    key = '^';
  } else {
    key = e.key;
  };
  cursor = '█';
  if ( key.length == 1 && cmd_str.length < 46 ) cmd_str += key;
  if ( keys[key] ) keys[key]();
  cmd.innerText = cmd_str + cursor;
});

// ## KEYS

const keys = {
 "Backspace" : function(){
    cmd_str = cmd_str.substring(0,cmd_str.length-1);
  },
  "Enter" : function(){
    if ( cmd_str.split(' ').join('') ) listen();
  }
};

// # LISTEN

function listen(){
  log(print(tokenize('@'+player.key + " > " + cmd_str),true,"input"));
  cmd_str = '';
};


// ## print tokens

function tokenize(str){

  let tokens = [];
 
  
  // catch potentials active tokens
  
  let active_tokens = [];
  
  // tokens are from player context

  for ( var i in token ) {
    let index = str.indexOf(i);
    if ( index > -1 ) {
      active_tokens.push([str.substring(index,index+i.length),index,index+i.length]);
    };
  };

  for ( var i in player.context.token ) {
    let index = str.indexOf(i);
    if ( index > -1 ) {
      active_tokens.push([str.substring(index,index+i.length),index,index+i.length]);
    };
  };

  // discard fake active tokens

  let to_remove = []  
  for ( var i = 0 ; i < active_tokens.length ; i++ ){
    for ( var j = 0 ; j < active_tokens.length ; j++ ){
      if(!active_tokens[i]) continue;
      if ( active_tokens[i][0].length
         > active_tokens[j][0].length
        && active_tokens[j][1]   
        >= active_tokens[i][1]
        && active_tokens[j][2]
        <= active_tokens[i][2] ) {
          to_remove.push(active_tokens[j]);
      };
    };
  };

  for ( var i = 0 ; i < to_remove.length ; i++ ){
    active_tokens.remove(to_remove[i]);
  }
  

  // dissect str in tokens 

  let separators = " .,:;?!'`\"/\\|_-+-*~&={}[]@()\n\r";
  var step = 0;
  
  loop1:
  for ( var i = 0 ; i < str.length ; i++ ) {
    for ( var j = 0 ; j < active_tokens.length ; j++ ) {
      if ( i == active_tokens[j][1] ) {
        i = active_tokens[j][2];
        break;
      };
    };
    let index = separators.indexOf(str[i]);
    if ( index > -1 ) {
      if(str.substring(step,i).length) tokens.push(str.substring(step,i));
      tokens.push(str.substring(i,i+1));
      step = i+1;
    };
  };
  if(str.substring(step,str.length).length) {
    tokens.push(str.substring(step,str.length));
  };
  

  return tokens;
};

function print(tokens,exec,from){
  let entry = []
  var line = document.createElement('p');
  var token_wrapper = document.createElement("span");
  // token_wrapper.innerText = "* ";
  token_wrapper.style.color = token_color[from] 
  line.appendChild(token_wrapper)
  for ( var i = 0 ; i < tokens.length ; i++ ){
    let token_wrapper = document.createElement("span");
    token_wrapper.style.color = token_color[from] 
    token_wrapper.textContent = tokens[i]
    loop0:
    for ( var j = 0; j < player.commands.length ; j++ ) {
      let command_tokens = player.commands[j].split(',');
      for ( var k = 0; k < command_tokens.length; k++ ){
        if ( command_tokens[k] == tokens[i] ) {
          token_wrapper.style.color = "#00f";
          if ( exec ) {
            setTimeout(function(){
              commands[this[1]](player,this[0].slice(this[0].indexOf(this[2])+2));
            }.bind([tokens,player.commands[j],tokens[i]]),100+Math.random()*200);
            exec = false;
          };
          break loop0;
        };
      };
    };
    // player context colorization
    if ( token_color[player.context.token[tokens[i]]] ) {
      token_wrapper.style.color = token_color[player.context.token[tokens[i]]];
    };
    if ( token[tokens[i]] ) {
      token_wrapper.style.color = token_color[token[tokens[i]]];
    };
    if ( line.innerText.length + token_wrapper.innerText.length > 45 || tokens[i][tokens[i].length-1] == '\n' ){
      entry.push(line)
      line = document.createElement('p');
      line.appendChild(token_wrapper);
    } else {
      line.appendChild(token_wrapper);
    };
  };
  entry.push(line);
  return entry;
}

// # ANIMATIONS

function cursor_blink(){
  if ( cursor == '█' ) { 
    cursor = ' ';
  } else {
    cursor = '█';
  };
  cmd.innerText = cmd_str + cursor;
  setTimeout(function(){cursor_blink();},1000); 
};

cursor_blink();

