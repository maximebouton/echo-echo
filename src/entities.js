const knowledge = {
  "common" : [
    { // lvl 1
      "goblin,goblins,Goblin,Goblins" : "Small sentient creatures living in humid areas with greensish / yellowish to brown skin"
    }
  ],
}

/*

  Sizes
  -----
  
  1 : item size
  2 : entity size
  3 : structure size
  4 : location size


*/

// const quests = {}

const Entity = function(name,type,size,quantity,descr,setting){
  this.name = name[0];
  this.quantity = quantity;
  this.key = name[0].split(' ')[name[1]];
  this.type = type;
  this.size = size;
  this.descr = descr; 
  this.setting = setting; 
  this.content = [];
  this.context = undefined;
  this.entities = {};
  this.token = {};
  this.links = [];
  this.quests = [];
  this.commands = [];
  this.relationship = {};
  
  // self tokenization
    
  if ( this.key ) {
    this.token[this.key.capitalize()] = this.type;
    this.token[this.key.toLowerCase()] = this.type;
    this.entities[this.key.toLowerCase()] = this;
    this.entities[this.key.capitalize()] = this;
  } else {
    this.token[this.name.capitalize()] = this.type;
    this.token[this.name.toLowerCase()] = this.type;
    this.entities[this.name.capitalize()] = this;
    this.entities[this.name.toLowerCase()] = this;
  };
};

Entity.prototype.remove = function(entity,nocontext){
  if ( this.content.indexOf(entity) < 0 ) return false;
  if ( entity.key ) {
    this.token[entity.key.capitalize()] = undefined;
    this.token[entity.key.toLowerCase()] = undefined;
    this.entities[entity.key.toLowerCase()] = undefined;
    this.entities[entity.key.capitalize()] = undefined;
  } else {
    this.token[entity.name.capitalize()] = undefined;
    this.token[entity.name.toLowerCase()] = undefined;
    this.entities[entity.name.capitalize()] = undefined;
    this.entities[entity.name.toLowerCase()] = undefined;
  };
  this.entities[entity.name.toLowerCase()] = undefined;
  this.entities[entity.name.capitalize()] = undefined;
  if (!nocontext) entity.context = undefined;
  if (!nocontext) this.content.remove(entity);
}

Entity.prototype.add = function(entity,nocontext){
  if ( this.content.indexOf(entity) > -1 ) return false;
  if ( entity.key ) {
    this.token[entity.key.capitalize()] = entity.type;
    this.token[entity.key.toLowerCase()] = entity.type;
    this.entities[entity.key.toLowerCase()] = entity;
    this.entities[entity.key.capitalize()] = entity;
  } else {
    this.token[entity.name.capitalize()] = entity.type;
    this.token[entity.name.toLowerCase()] = entity.type;
    this.entities[entity.name.capitalize()] = entity;
    this.entities[entity.name.toLowerCase()] = entity;
  };
  this.entities[entity.name.toLowerCase()] = entity;
  this.entities[entity.name.capitalize()] = entity;
  if (!nocontext) entity.context = this;
  if (!nocontext) this.content.push(entity);
  if (entity == player ) {
    document.body.style.backgroundColor = location_colors[player.context.name][0];
    document.body.style.color = location_colors[player.context.name][1];
  };
}

Entity.prototype.skills = function(filter){
  if ( !filter.length ) filter = "skill";
  var to_return = "";
  loop1:
  for ( var i in command_list ) {
    for ( var j = 0 ; j < this.commands.length ; j++ ){
      if ( this.commands[j].indexOf(i) > -1 ) {
        if ( filter != "skill" && command_list[i] != filter ) continue
        to_return += i + ', ';
        continue loop1;
      };
    };
  };
  to_return = to_return.slice(0,to_return.length-2)
  return to_return
}

Entity.prototype.learn = function(name){
  for ( var i in commands ) {
    let synonyms = i.split(',');
    for ( var j = 0; j < synonyms.length; j++ ){
      if ( name == synonyms[j] ){
        this.commands.push(i);
      };
    };
  }
  return "You learn how to "+name+" !\n Enter \"> about "+name+"\" to know more.";
};

Entity.prototype.go = function(options){
  entity = options.join('');
  console.log(entity);
  if ( !entity || !this.context.entities[entity.toLowerCase()]  ) return "Go where ?";
  if (!entity.key) entity = this.context.entities[entity.toLowerCase()];
  if ( entity.content.indexOf(this) > -1 ) return "You're already there !";
  if ( this.size >= entity.size ) return "You're too large to go in there !";
  if ( this.context.links.indexOf(entity) == -1 ) return "You can't go there !";
  if ( this.context ) this.context.remove(this);
  entity.add(this);
  return "You went to " + entity.name + '.';
};

Entity.prototype.look = function(options){
  entity = options.join('');
  console.log(entity)
  if (!entity){
    let descr = "";
    let content = [];
    for ( var i = 0 ; i < this.context.links.length ; i++ ){
      content.push(this.context.links[i]);
    };
    for ( var i = 0 ; i < this.context.content.length ; i++ ){
      content.push(this.context.content[i]);
    };
    content.remove(this);
    descr += "You are located in " + this.context.name + ". " 
    + ( content.length ? "You see around you " : "You see nothing around you." );
    for ( var i = 0; i < content.length ; i++ ){
      descr += content[i].name 
             + ( content[i].setting 
               ? ( content[i].setting[this.context.name] ? ' '+content[i].setting[this.context.name] : 
                 ( content[i].setting["everywhere"] ? ' ' +content[i].setting["everywhere"] : '' ) )
               : '' ) 
             + ( i < content.length-2 ? ", " : i == content.length-1 ? '.' : " and " );
    };
    return descr;
  } else {
    if ( !this.context.entities[entity.toLowerCase()] ) return "Look at what ?";
    if (!entity.key) entity = this.context.entities[entity.toLowerCase()];
    return "You see "+ (entity.descr ? entity.descr : "nothing in particular") +'.'
  }
};

Entity.prototype.talk = function(options){
  entity = options.join('');
  if ( !entity || !this.context.entities[entity.toLowerCase()] ) return "Talk to who ?";
  if (!entity.key) entity = this.context.entities[entity.toLowerCase()];
  
  var dialog_index ;

  for ( var i in dialogs ){
    if ( i.indexOf( entity.key ) > -1 ) dialog_index = i;
  };

  if ( !dialogs[dialog_index] ) return "This entity can't talk";
  if ( !this.relationship[entity.key] ) this.relationship[entity.key] = 0;
  let to_return = entity.key +" > "+ dialogs[dialog_index][this.relationship[entity.key]][0];
  if( dialogs[dialog_index][this.relationship[entity.key]][2] ) {
    dialogs[dialog_index][this.relationship[entity.key]][2]();
  };
  this.relationship[entity.key] += dialogs[dialog_index][this.relationship[entity.key]][1];
  return to_return
};

Entity.prototype.about = function(name){
  if ( !name.length ) name = ["about"];
  if ( token[name] == "command" ){
    var index
    for ( var i = 0 ; i < this.commands.length ; i++ ) {
      let synonyms = this.commands[i].split(',');
      for ( var j = 0; j < synonyms.length; j++ ){
        if ( name == synonyms[j] ){
          index = this.commands[i];
        };
      };
    };
    return man[index].about;
  } else if ( token[name] == "subject" ) {
    return knowledge.common[0]
  }
}


// entities relative function

function put(what,where){
  if ( !what || !where ) return "Put what, where ?";
  if ( where.size <= what.size ) return "This is is too large to be put there !";
  if ( what.context ) what.context.remove(this);
  where.add(what);
  return "You put "+what.name+" in "+where.name+'.';
};

function link(from,to,mutual){
  if ( !from || !to ) return "Link what ? To where ?";
  if ( from.size != to.size ) return "You can't link entities of different scales !";
  from.links.push(to);
  from.add(to,true);
  if ( mutual ) { 
    to.links.push(from);
    to.add(from,true);
  }
  return "You link "+from+" to "+to+(mutual?"(mutual).":'.');
};


// Quests

const Quest = function(name,descr,objective,result){
  this.name = name;
  this.descr = descr; 
  this.objective = objective; 
 
  token[this.name] = "quest";
  token[this.name.toLowerCase()] = "quest";
  //quests[this.name.toLowerCase()] = this;
  setTimeout(function(){
    log(print(tokenize("* New quest : \""+this.name+"\" !")),false,"game");
  }.bind(this),Math.random()*100+100);
}


// world construction

const location_colors = {
  "Rene's hut" : ["#630","#bbb"],
  "a small hill in the swamps" : ["#cfe","#300"],
  "the great swamps" : ["#064","#bbb"]
};

/* entity declaration template
  const entity = new Entity(
    [str:full name, int:key],
    str:type,
    int:size,
    int:quantity,
    str:descr,
    obj:setting
  );
*/
const great_swamps = new Entity(
  ["the great swamps", -1],
  "location",
  4,
  1,
  "a vast flooded region",
  {"a small hill in the swamps":"surrounding you"}
);

const rene = new Entity(["Rene the old goblin",0],"npc",2,1,"an old goblin whose body seems to be shaped by a life of solitude in the swamps",{"Rene's hut":"sitting in a chair"});
const rene_stove = new Entity(["Rene's rusty stove",2],"structure",2,1,"an old stove made of junk");

const dialogs = {
  "Rene" : [
    [
      "Hello soft-skinned ( Rene doesn't seem to recognize you ), would you like to help an old goblin ? I can barely get up on my feet and my fire is cold…  Can you be kind and put some logs in my stove ?",
      1,
      function(){
        player.quests.push(new Quest("Logs for an old goblin","Rene the old goblin living in the swamps ask you to put logs in his stove",[rene_stove.content,5,"willow log"],function(){
           player.relationship["Rene"]++;
        }));
      }
    ],[
      "( Rene is smiling at you, eyes almost closed. )",
      0
    ],
  ],
};



const player = new Entity(["Tommie the human",0],"player",2,1);
player.learn("about");
player.learn("skill");


const renes_hut = new Entity(["Rene's hut",-1],"location",4,1,"a small shelter made of terracotta");
const swamps_hill = new Entity(["a small hill in the swamps",2],"location",4,1,"what seems to be the only piece of real estate for miles around",
                          {
                            "Rene's hut" : "visible throught the hut entrance",
                            "the great swamps" : "just above the mist",
                          });

link(renes_hut,swamps_hill,true);
link(swamps_hill,great_swamps,true);
put(player,renes_hut);
put(rene,renes_hut);
put(rene_stove,renes_hut);
put(new Entity(["willow log",-1],"item",2,2,"some piece of wood",
          {
            "everywhere" : "on the ground"
          }),swamps_hill);

log(print(tokenize("* "+player.learn("look"),false,"game")));
log(print(tokenize("* "+player.learn("go"),false,"game")));
log(print(tokenize("* "+player.learn("talk"),false,"game")));
