/* COMMON
   ======
  

   Base functions modification and utils
   ------------------------------------- */

Array.prototype.remove = function(obj){
    let index = this.indexOf(obj);
    if (index < 0) return;
    let to_concat = this.splice(index);
    let new_arr = this.concat(to_concat.splice(1));
    this.splice(0);
    for ( var i = 0; i < new_arr.length; i++ ){
        this.push(new_arr[i]);
    };
};

Array.prototype.rdm = function(){
  return this[parseInt(Math.random()*this.length)]
}

String.prototype.capitalize = function(){
  return this.replace(/^\w/, c => c.toUpperCase());
}

function pos(str){
    return str.split(',').map(Number)
}

function get(selector){
    return document.querySelector(selector)
}

function create(tag){
    return document.createElement(tag);
}
