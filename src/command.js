const man = {
  "about,About" : {
    "about" : "\"knowledge\" command\n[ about, About ] ( command name, subject, \"quests\" or a quest name )\n - Display informations about a command, list current quests, give informations about a specific quest or give specific and useful informations about a game lore subject depending on your knowledge aptitude.\n- Examples :\n\"> about look\"\n\"> About Goblins\""
  },
  "look,Look,look at,Look at" : {
    "about" : "\"perception\" command\n[ look, Look, look at, Look at ] ( no arguments or entity name )\n - Wihout arguments, display the name, type and additional informations of the different entities present in your location. With an entity name as argument can give specific and useful informations depending on your perception aptitude.\n- Examples :\n\"> look\"\n\"> Look at painting\""
  },
  "go,go to,Go,Go to" : {
    "about" : "\"orientation\" command\n[ go, go to, Go, Go to ] ( entity name )\n - Move in a location nearby.\n- Examples :\n\"> go north\"\n\"> Go to Plains\""
  },
  "talk,talk to,Talk,Talk to" : {
    "about" : "\"charizma\" command\n[ talk, talk to, Talk, Talk to ] ( entity name )\n - Interact with a nearby sentient entity.\n- Examples :\n\"> talk Celeste the dwarf\"\n\"> Talk to Dallas the orc\""
  },
  "skill,Skill" : {
    "about" : "\"knowledge\" command\n[ skills, Skills ] ( no arguments or filter )\n - List known commands, no filter will list all of them. Filters are : \"charizma, knowledge, orientation, perception\".\n- Examples :\n\"> skill\"\n\"> Skill knowledge\""
  }
};

const commands = {

  "about,About":function(entity,options){
    log(print(tokenize("* "+entity.about(options))),false,"game");
  },

  "look,Look,look at,Look at":function(entity,options){
    log(print(tokenize("* "+entity.look(options))),false,"game");
  },

  "go,go to,Go,Go to":function(entity,options){
    log(print(tokenize("* "+entity.go(options))),false,"game");
  },

  "talk,talk to,Talk,Talk to":function(entity,options){
    log(print(tokenize("* "+entity.talk(options))),false,"game");
  },
  
  "skill,Skill":function(entity,options){
    log(print(tokenize("* "+entity.skills(options))),false,"game");
  },

};

const command_list = {
  "about" : "knowledge",
  "look" : "perception",
  "go" : "orientation",
  "talk" : "charizma",
  "skill" : "knowledge"
};

for ( var i in commands ) {
  let synonyms = i.split(',')
  for ( var j = 0 ; j < synonyms.length ; j++ ) {
    token[synonyms[j]] = "command";
  }
};
