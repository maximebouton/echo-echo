- [0.6] look allow to display description of entities, each location trigger a different background color.
- [0.5] Adding some text animation and fake delays, potentials active tokens are now correctly catch and discard when fake ( there was a bug for displaying long tokens containing smallers one deleting all active tokens ).
- [0.4] Multi tokens, skip, help without arguments commands work again. Universal setting for entities. 
- [0.3] nickname / name system are replaced with a name / key system, tokenization depends now on the player context
- [0.2] commands synonyms, entities nickname, quest system, entities synonyms, dialog system, entities setting, command learning, help command, in-game manual, skill command
- [0.1] Basic command-line interface with colorizes tokens and look, talk commands


## For next version


## More to come ( in priority order )

- about command must allow to list quests and get information about them
- about command must allow to check self knowledge about entities
- knowledge must be divided into common and class specific informations
- entities can have quantity, 2 entities of the same name in same content disolve into quantity in the previous entity present in context
- take / put command
- content limit for entities
- look feedback pertinence (interactive détails, hidden entities) must be increase by perception aptitude
- about feedback pertinence (game tips and functionnal informations) must be increase by knowledge aptitude
- tutorial what how and mhy to type


## Textual feedback design

- help undefined dont give feedback ( delete all error feedback ? rationalize them like sound feedback for options error or incompatibilities
